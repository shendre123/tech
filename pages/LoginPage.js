exports.LoginPage=

class LoginPage{
    constructor(page)
    {
        this.page=page;
        this.username='#user-name';
        this.password='#password';
        this.login="//input[@id='login-button']";
    }
    async doLogin(username,password){
        await this.page.locator(this.username).fill(username);
        await this.page.locator(this.password).fill(password);
        await this.page.locator(this.login).click();
    }

}