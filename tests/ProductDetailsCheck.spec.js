import {test,expect} from '@playwright/test';
import { LoginPage } from '../pages/LoginPage';
import {HomePage} from '../pages/HomePage';
import { ProductPage } from '../pages/ProductPage';

test('Product Details verification on Homepage and Product page',async({page})=>{
   //override the existing timeout 
   test.setTimeout(0);
    //login to the applicationS
   await page.goto('/');

   const loginPage=new LoginPage(page);
   await loginPage.doLogin("problem_user","secret_sauce");
   //overriding the time out in test
   await expect(page.getByText("Products")).toBeVisible({timeout : 0});

   //Home Page
   const homePage=new HomePage(page);
   const productPage=new ProductPage(page);
  
   const links=await homePage.getAllProductLinks();
   const attachedText=await homePage.getAllLinksAttachedText();
   const attachedImage=await homePage.getAllLinksAttachedImage();

   for (let i=0 ; i< await links.length; i++)
    {
      const productname=await links[i].innerText();
      const expectedText=await attachedText[i].innerText();
      const expectedImage=await attachedImage[i].getAttribute("alt")

      await homePage.clickProductLink(productname); 
      //product and verify page
      test.step("Verify product links on item page",async()=>{
         const productLinkName=await productPage.getProductLinkName();
         await expect.soft(productLinkName).toEqual(productname)
      })
      
      test.step("Verify product details on item page",async()=>{
         const prouctDetails=await productPage.getProductDetails()
         await expect.soft(prouctDetails).toEqual(expectedText)
      })
      
      test.step("Verify product image on item page",async()=>{
         const imageDetails=await productPage.getProductImage()
         await expect.soft(imageDetails).toEqual(expectedImage);
      })
      //verification of product details
            
      await productPage.clickBackToProductLink();
      await page.locator('//span[@class="title"]').waitFor("visible")
   }

   await homePage.doLogout();
   await page.close();

})

test.describe("Sanity Tests",()=>{

   test("Test1",()=>{
      console.log("This is test 1")
   })

   test("test 2",()=>{
      console.log("This is test 2")

   })
})
