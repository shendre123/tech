const {test,expect}=require ('@playwright/test');
const { isTypedArray } = require('util/types');

test("API testing",async({request})=>{

    const response=await request.get("https://dog.ceo/api/breeds/list/all")

    const jsonObject=await response.json();
    const arrayMessage=await jsonObject.message;
    const arrKeys=Object.keys(arrayMessage)
    const arrValues=Object.values(arrayMessage)

   for(let i=0 ; i<await arrKeys.length ; i++)
    {
        if (await arrValues[i].length == 0){
            console.log(`A ${await arrKeys[i]} breed has no sub breed`)
        }
        else {
            if(await arrValues[i].length > 1){
                console.log(`A ${await arrKeys[i]} breed has many sub breeds`)
            }
            //subbreed can not have sub breed
            const subBreed=arrValues[i];
      }
    }
})


test.describe("Sanity Tests",()=>{
    test("Test 3",()=>{
        console.log("This is test 3")
    })

    test("Test 4",()=>{
        console.log("This is test 4")
    })
})